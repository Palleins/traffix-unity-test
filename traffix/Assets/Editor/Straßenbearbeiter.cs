﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Data;
using System.Data.SqlClient;

[CustomEditor(typeof(straßenbauer))]
public class Straßenbearbeiter : Editor
{
    DataTable liste_nodes = new DataTable("nodes");
    DataTable liste_weg = new DataTable("weg");
    DataTable closed = new DataTable("zuauf");
    bool init = true;

    public float abstand = 0.1f;
    public float genauigkeit = 1;

    straßenbauer bauer;
    public Street straße;

    float max_abstand_zu_abschnitt = 0.2f;
    int ausgewählter_abschnitt = -1;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("neuer Weg"))
        {
            Undo.RecordObject(bauer, "Neuer Weg");
            bauer.bau_straße();
            straße = bauer.straße;
            draw();
        }

        bool geschlossen = GUILayout.Toggle(straße.Closed, "Weg schließen");
        if (geschlossen != straße.Closed)
        {
            Undo.RecordObject(bauer, "Straße zu/auf");
            straße.Closed = geschlossen;
            draw();
        }

        bool set_kontrollpunkte = GUILayout.Toggle(straße.Setzen_von_kontrollpunkten, "Kontrollpunkte setzen");
        if (set_kontrollpunkte != straße.Setzen_von_kontrollpunkten)
        {
            Undo.RecordObject(bauer, "Kontrollpunkte setzen an/aus");
            straße.Setzen_von_kontrollpunkten = set_kontrollpunkte;
            draw();
        }

        bool nodesmalen = GUILayout.Toggle(straße.Nodesmalen, "Nodes malen");
        if (nodesmalen != straße.nodesmalen)
        {
            Undo.RecordObject(bauer, "Nodes malen an/aus");
            straße.Nodesmalen = nodesmalen;
            draw();
        }


    }
    void OnSceneGUI() //wenn man auf dem GUI ist, wird der Kram durchgehend ausgeführt
    {
        if (init)
        {
            liste_nodes.Columns.Add("X", typeof(float));
            liste_nodes.Columns.Add("Y", typeof(float));
            liste_weg.Columns.Add("X", typeof(float));
            liste_weg.Columns.Add("Y", typeof(float));
            closed.Columns.Add("aufzu", typeof(bool));
            init = false;
        }
        Input();
        draw();
    }
    void Input()
    {
        Event guievent = Event.current; //= das current guiEvent, duh
        Vector2 mousepos = HandleUtility.GUIPointToWorldRay(guievent.mousePosition).origin; //returned die Mauspostion (probleme mit 3d-Welt, Code aus dem Inernet, keinen Schimmer
        if (guievent.type == EventType.MouseDown && guievent.button == 0 && guievent.shift) //wenn mausdown, und Linksclick, und shift gedrückt
        {
            if (ausgewählter_abschnitt != -1)
            {
                Undo.RecordObject(bauer, "neuen Abschnitt eingefügt");
                straße.punkt_zwischen_schieben(mousepos, ausgewählter_abschnitt);
            }
            else if (!straße.Closed) //nur bei offener Straße erlaubt
            {
                Undo.RecordObject(bauer, "neuen Abschnitt eingefügt");
                straße.abschnitt_hinzufügen(mousepos, "Curve");
            }
        }
        if (guievent.type == EventType.MouseDown && guievent.button == 1 && guievent.shift && !straße.Closed) //Mit Rechtsclick einen Punkt Löschen
        {
            Undo.RecordObject(bauer, "neuen Abschnitt eingefügt");
            straße.abschnitt_hinzufügen(mousepos, "Line");
        }
        if (guievent.type == EventType.MouseDown && guievent.button == 1) //wenn mausdown, und RECHTSclick, und shift gedrückt
        {

            float minabstand = 0.05f; //genauigkeit des Clicks
            int index = -1; //index des gültigen Ankerpunkts -> -1 für Kontrolle am Ende
            for (int i = 0; i < straße.anzahl_punke; i += 3)
            {
                float distanz = Vector2.Distance(mousepos, straße[i]); //Abstand Maus zu Punkt
                if (distanz < minabstand) //Click nahe genug
                {
                    // Debug.Log("test");
                    minabstand = distanz; //falls mehrere ganz nahe sind
                    index = i; //index merken
                }
            }
            if (index != -1) //wurde überhaupt einer angeclickt
            {
                Undo.RecordObject(bauer, "Abschnitt löschen");
                straße.AbschnittLöschen(index); //löschen
            }
        }

        if (guievent.type == EventType.MouseMove)
        {
            float min_abstand = max_abstand_zu_abschnitt;
            int neuer_ausgewählter_abschnitt = -1;
            for (int i = 0; i < straße.anzahl_abschnitte; i++)
            {
                Vector2[] points = straße.get_abschnitt(i);
                float abstand = HandleUtility.DistancePointBezier(mousepos, points[0], points[3], points[1], points[2]);
                if (abstand <= min_abstand)
                {
                    min_abstand = abstand;
                    neuer_ausgewählter_abschnitt = i;
                }
                if (neuer_ausgewählter_abschnitt != ausgewählter_abschnitt)
                {
                    ausgewählter_abschnitt = neuer_ausgewählter_abschnitt;
                    draw();
                }
            }
        }

    }
    void draw()
    {
        if (init)
        {
            liste_nodes.Columns.Add("X", typeof(float));
            liste_nodes.Columns.Add("Y", typeof(float));
            liste_weg.Columns.Add("X", typeof(float));
            liste_weg.Columns.Add("Y", typeof(float));
            closed.Columns.Add("aufzu", typeof(bool));
            init = false;
        }

        for (int i = 0; i < straße.anzahl_abschnitte; i++) //malt die Strecke
        {
            Vector2[] points = straße.get_abschnitt(i);
            if (bauer.kontrollpunkte_anzeigen)
            {
                Handles.color = Color.gray;
                Handles.DrawLine(points[1], points[0]);
                Handles.DrawLine(points[2], points[3]);
            }
            Color farbe = (i == ausgewählter_abschnitt && Event.current.shift ? bauer.ausgewählter_abschnitt : bauer.abschnitt);
            Handles.color = farbe;
            Handles.DrawBezier(points[0], points[3], points[1], points[2], farbe, null, 2f);
            
        }

        liste_weg.Clear();
        for (int i = 0; i < straße.anzahl_punke; i++) //malt die Punkte
        {
            Handles.color = bauer.kontrollpunkt;
            if (i % 3 == 0)
            {
                Handles.color = bauer.anker;
            }
            if (i % 3 == 0 || bauer.kontrollpunkte_anzeigen)
            {
                Vector2 neue_pos = Handles.FreeMoveHandle(straße[i], Quaternion.identity, 0.1f, Vector3.zero, Handles.CylinderHandleCap); //Punkt bewegen
                if (neue_pos != straße[i]) //neue Position anders?
                {
                    Undo.RecordObject(bauer, "Punkt wurde bewegt"); //speichern der Bewegung
                    straße.punkt_bewegen(i, neue_pos); //Neue Position übergeben
                }
            }
                DataRow weg = liste_weg.NewRow();
                weg["X"] = straße[i].x;
                weg["Y"] = straße[i].y;
            liste_weg.Rows.Add(weg);
        }
        liste_weg.WriteXml("streckenpunkte.xml");



        //alte sphere löschen
            GameObject[] spheres = GameObject.FindGameObjectsWithTag("sphere"); //alle alten suchen
            foreach (var item in spheres)
            {
                DestroyImmediate(item); //alte wieder löschen
            }


        liste_nodes.Clear();
        Vector2[] p = FindObjectOfType<straßenbauer>().straße.nodes_berechen(abstand, genauigkeit);
        foreach (Vector2 pu in p)
        {
            if (straße.Nodesmalen)
            {
                //Spheren an der Stelle der Nodes malen
                GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere); //sphere erstellen
                go.tag = "sphere"; //tagzum Suchen -> WICHTIG, sonst kann man sie nicht mhr löschen und sie bleiben für immer da
                go.transform.position = pu; 
                go.transform.localScale = Vector3.one * abstand * 0.5f;
            }

            DataRow node = liste_nodes.NewRow();
            node["X"] = pu.x;
            node["Y"] = pu.y;
            liste_nodes.Rows.Add(node);
        }
        liste_nodes.WriteXml("nodes.xml");

        closed.Clear();
        DataRow closedjanein = closed.NewRow();
        closedjanein["aufzu"] = straße.Closed;
        closed.Rows.Add(closedjanein);
        closed.WriteXml("closed.xml");
    }
    void OnEnable()
    {
        bauer = (straßenbauer)target;
        if (bauer.straße == null)
        {
            bauer.bau_straße();
        }
        straße = bauer.straße;
    }
    public Street get_straße()
    {
        return straße;
    }
}
