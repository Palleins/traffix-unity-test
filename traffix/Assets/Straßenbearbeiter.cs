﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(straßenbauer))]
public class Straßenbearbeiter : Editor
{
    straßenbauer bauer;
    Street straße;

    float max_abstand_zu_abschnitt = 0.2f;
    int ausgewählter_abschnitt = -1;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("neuer Weg"))
        {
            Undo.RecordObject(bauer, "Neuer Weg");
            bauer.bau_straße();
            straße = bauer.straße;
            draw();
        }

        bool geschlossen = GUILayout.Toggle(straße.Closed, "Weg schließen");
        if (geschlossen != straße.Closed)
        {
            Undo.RecordObject(bauer, "Straße zu/auf");
            straße.Closed = geschlossen;
            draw();
        }

        bool set_kontrollpunkte = GUILayout.Toggle(straße.Setzen_von_kontrollpunkten, "Kontrollpunkte setzen");
        if (set_kontrollpunkte != straße.Setzen_von_kontrollpunkten)
        {
            Undo.RecordObject(bauer, "Kontrollpunkte setzen an/aus");
            straße.Setzen_von_kontrollpunkten = set_kontrollpunkte;
            draw();
        }



    }
    void OnSceneGUI() //wenn man auf dem GUI ist, wird der Kram durchgehend ausgeführt
    {
        Input();
        draw();
    }
    void Input()
    {
        Event guievent = Event.current; //= das current guiEvent, duh
        Vector2 mousepos = HandleUtility.GUIPointToWorldRay(guievent.mousePosition).origin; //returned die Mauspostion (probleme mit 3d-Welt, Code aus dem Inernet, keinen Schimmer
        if (guievent.type == EventType.MouseDown && guievent.button == 0 && guievent.shift) //wenn mausdown, und Linksclick, und shift gedrückt
        {
            if (ausgewählter_abschnitt != -1)
            {
                Undo.RecordObject(bauer, "neuen Abschnitt eingefügt");
                straße.punkt_zwischen_schieben(mousepos, ausgewählter_abschnitt);
            }
            else if (!straße.Closed) //nur bei offener Straße erlaubt
            {
                Undo.RecordObject(bauer, "neuen Abschnitt eingefügt");
                straße.abschnitt_hinzufügen(mousepos, "Curve");
            }
        }
        if (guievent.type == EventType.MouseDown && guievent.button == 1 && guievent.shift && !straße.Closed) //Mit Rechtsclick einen Punkt Löschen
        {
            Undo.RecordObject(bauer, "neuen Abschnitt eingefügt");
            straße.abschnitt_hinzufügen(mousepos, "Line");
        }
        if (guievent.type == EventType.MouseDown && guievent.button == 1) //wenn mausdown, und RECHTSclick, und shift gedrückt
        {

            float minabstand = 0.05f; //genauigkeit des Clicks
            int index = -1; //index des gültigen Ankerpunkts -> -1 für Kontrolle am Ende
            for (int i = 0; i < straße.anzahl_punke; i += 3)
            {
                float distanz = Vector2.Distance(mousepos, straße[i]); //Abstand Maus zu Punkt
                if (distanz < minabstand) //Click nahe genug
                {
                   // Debug.Log("test");
                    minabstand = distanz; //falls mehrere ganz nahe sind
                    index = i; //index merken
                }
            }
            if (index != -1) //wurde überhaupt einer angeclickt
            {
                Undo.RecordObject(bauer, "Abschnitt löschen");
                straße.AbschnittLöschen(index); //löschen
            }
        }

        if (guievent.type == EventType.MouseMove)
        {
            float min_abstand = max_abstand_zu_abschnitt;
            int neuer_ausgewählter_abschnitt = -1;
            for (int i = 0; i < straße.anzahl_abschnitte; i++)
            {
                Vector2[] points = straße.get_abschnitt(i);
                float abstand = HandleUtility.DistancePointBezier(mousepos, points[0], points[3], points[1], points[2]);
                if (abstand <= min_abstand)
                {
                    min_abstand = abstand;
                    neuer_ausgewählter_abschnitt = i;
                }
                if (neuer_ausgewählter_abschnitt != ausgewählter_abschnitt)
                {
                    ausgewählter_abschnitt = neuer_ausgewählter_abschnitt;
                    draw();
                }
            }
        }

    }
    void draw()
    {
        for (int i = 0; i < straße.anzahl_abschnitte; i++) //malt die Strecke
        {
            Vector2[] points = straße.get_abschnitt(i);
            if (bauer.kontrollpunkte_anzeigen)
            {
                Handles.color = Color.gray;
                Handles.DrawLine(points[1], points[0]);
                Handles.DrawLine(points[2], points[3]);
            }
            Color farbe = (i == ausgewählter_abschnitt && Event.current.shift ? bauer.ausgewählter_abschnitt : bauer.abschnitt);
            Handles.color = farbe;
            Handles.DrawBezier(points[0], points[3], points[1], points[2], farbe, null, 2f);
        }
        for (int i = 0; i < straße.anzahl_punke; i++) //malt die Punkte
        {
            Handles.color = bauer.kontrollpunkt;
            if (i % 3 == 0)
            {
                Handles.color = bauer.anker;
            }
            if (i % 3 == 0 || bauer.kontrollpunkte_anzeigen)
            {
                Vector2 neue_pos = Handles.FreeMoveHandle(straße[i], Quaternion.identity, 0.1f, Vector3.zero, Handles.CylinderHandleCap); //Punkt bewegen
                if (neue_pos != straße[i]) //neue Position anders?
                {
                    Undo.RecordObject(bauer, "Punkt wurde bewegt"); //speichern der Bewegung
                    straße.punkt_bewegen(i, neue_pos); //Neue Position übergeben
                }
            }
            
        }
    }
    void OnEnable()
    {
        bauer = (straßenbauer)target;
        if (bauer.straße == null)
        {
            bauer.bau_straße();
        }
        straße = bauer.straße;
    }
}
