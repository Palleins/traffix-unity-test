﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class straßenbauer : MonoBehaviour
{
    [HideInInspector]
    public Street straße;

    public bool kontrollpunkte_anzeigen = true;

    public Color anker = Color.red;
    public Color kontrollpunkt = Color.blue;
    public Color abschnitt = Color.magenta;
    public Color ausgewählter_abschnitt = Color.green;


    public void bau_straße()
    {
        straße = new Street(transform.position);
    }
}
