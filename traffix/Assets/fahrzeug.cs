﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEditor;

public class fahrzeug : MonoBehaviour
{
    public float abstand = 0.1f;
    public float genauigkeit = 1;
    List<Vector2> nodes = new List<Vector2>();
    List<Vector2> weg = new List<Vector2>();

    float zurückgelegt = 0;
    float speed = 1f;
    int nächste_node = 1;

    DataTable liste_nodes = new DataTable("nodes");
    DataTable liste_weg = new DataTable("weg");
    void Start()
    {
        liste_nodes.Columns.Add("X", typeof(float));
        liste_nodes.Columns.Add("Y", typeof(float));
        liste_nodes.ReadXml("nodes.xml");
        foreach (DataRow item in liste_nodes.Rows)
        {
            Vector2 neue_node = new Vector2(Convert.ToSingle(item["X"].ToString()), Convert.ToSingle(item["Y"].ToString()));
            nodes.Add(neue_node);
        }
        liste_weg.Columns.Add("X", typeof(float));
        liste_weg.Columns.Add("Y", typeof(float));
        liste_weg.ReadXml("streckenpunkte.xml");
        foreach (DataRow item in liste_weg.Rows)
        {
            Vector2 punkt = new Vector2(Convert.ToSingle(item["X"].ToString()), Convert.ToSingle(item["Y"].ToString()));
            weg.Add(punkt);
            Debug.Log(punkt.x);
        }
        transform.position.Set(nodes[0].x, nodes[0].y, transform.position.z);
        //test();
        InvokeRepeating("bewegen", 0.0f, 0.005f);
    }
    void Update()
    {

    }
    int zähler = 1;
    Vector2 richtung;
    int num_moves = 5;
    void bewegen()
    {
        if (zähler == 1)
        {
            richtung = (nodes[index_loop(nächste_node)] - (Vector2)transform.position) / num_moves;
        }
        zähler++;
        Vector3 new_pos = new Vector3(transform.position.x + richtung.x, transform.position.y + richtung.y, transform.position.z);
        transform.position = new_pos;
        if (zähler == num_moves)
        {
            nächste_node++;
            zähler = 1;
        }
        
    }

    public int index_loop(int index)
    {
        return index % (nodes.Count - 1);
    }
    int weg_loop(int index)
    {
        return index % (weg.Count - 1);
    }

    public void test()
    {
        Handles.color = Color.red;
        //for (int i = 0; i < FindObjectOfType<straßenbauer>().straße.anzahl_abschnitte; i++) //malt die Strecke
        //{
        //   Vector2[] points = FindObjectOfType<straßenbauer>().straße.get_abschnitt(i);
        //   Handles.DrawBezier(points[0], points[3], points[1], points[2], Color.red, null, 2f);
        //}
        for (int i = 0; i < weg.Count-1; i+=3)
        {
            //Vector2[] abschnitt = { weg[i * 3], weg[i * 3 + 1], weg[i * 3 + 2], weg[weg_loop(i * 3 + 3)] };
            Vector2[] abschnitt = { new Vector2(i * 0.5f, 0), new Vector2(i * 0.5f, 0), new Vector2(i * 0.5f, 0), new Vector2(i * 0.5f, 0) };
            Handles.DrawBezier(abschnitt[0], abschnitt[3], abschnitt[1], abschnitt[2], Color.red, null, 2f);
        }
    }
    
}
