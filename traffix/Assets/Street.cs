﻿using Boo.Lang.Environments;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

[System.Serializable]
public class Street : MonoBehaviour
{
    [SerializeField, HideInInspector]
    public List<Vector2> points;
    [SerializeField, HideInInspector]
    bool closed;
    [SerializeField, HideInInspector]
    bool setzen_von_kontrollpunkten;
    public Street(Vector2 mitte)
    {
        // die ersten 4 Punkte werden generiert, und um die angeclickte Mitte herum plaziert -> siehe Bild
        points = new List<Vector2>
        {
            mitte + Vector2.left,
            mitte + (Vector2.left + Vector2.up) * 0.5f,
            mitte + Vector2.right,
            mitte + (Vector2.right + Vector2.down) * 0.5f
        };
    }
    public int anzahl_abschnitte //gibt anzahl der abschnitte aus
    {
        get
        {
            if (closed)
            {
                return (points.Count - 4) / 3 + 1 + 1; //anzahl der Punkte - die ersten vier, geteilt durch die Punkte pro Abschnitt (3), plus den ersten Abschnitt, plus den geschlossenen
            }
            else
            {
                return (points.Count - 4) / 3 + 1; //ohne den schließenden
            }

        }
    }
    public int anzahl_punke //gibt anzahl der Punkte aus
    {
        get
        {
            return points.Count;
        }
    }
    public Vector2 this[int i] //gibt einen bestimmten Pubkt aus
    {
        get
        {
            return points[i];
        }
    }

    public void abschnitt_hinzufügen(Vector2 wegpunkt, string type) //für jeden neuen Abschnitt nach dem ersten
    {
        if (type == "Curve")
        {
            points.Add(points[points.Count - 1] * 2 - points[points.Count - 2]);
            points.Add(points[points.Count - 1] - (points[points.Count - 1] - wegpunkt) * 0.5f);
            points.Add(wegpunkt);
        }
        if (type == "Line")
        {
            Vector2 rechts_offset = new Vector2(0.1f, 0);
            points.Add(points[points.Count - 1] + rechts_offset);
            points.Add(wegpunkt + rechts_offset);
            points.Add(wegpunkt);
        }

        //ich glaube, hier zerschieße ich die gerade

        if (setzen_von_kontrollpunkten)
        {
            anker_verschoben(points.Count - 1); //letzter Anker
        }

    }
    public Vector2[] get_abschnitt(int index)
    {
        return new Vector2[] { points[index * 3], points[index * 3 + 1], points[index * 3 + 2], points[index_von_vorne(index * 3 + 3)] };
    }
    public void punkt_bewegen(int index, Vector2 neue_pos)
    {
        Vector2 bewegung = neue_pos - points[index];
        if (index % 3 == 0 || !setzen_von_kontrollpunkten)
        {
            points[index] = neue_pos;

            if (setzen_von_kontrollpunkten)
            {
                anker_verschoben(index);
            }
            else
            {

                if (index % 3 == 0) //Ankerpunkt -> die beiden "Kontrollpunkte" mitbewegen
                {
                    if (index + 1 < points.Count || closed)
                    {
                        points[index_von_vorne(index + 1)] += bewegung;
                    }
                    if (index - 1 >= 0)
                    {
                        points[index_von_vorne(index - 1)] += bewegung;
                    }
                }
                else //Kontrollpunkt bewegt
                {
                    bool next = (index + 1) % 3 == 0; //ist der Ankerpunkt vor oder nach dem Kontrollpunkt
                    int index_next_Kontrollpunkt = 0;
                    int index_anker = 0;
                    if (next) //danach
                    {
                        index_next_Kontrollpunkt = index + 2;
                        index_anker = index + 1;
                    }
                    else //davor
                    {
                        index_next_Kontrollpunkt = index - 2;
                        index_anker = index - 1;
                    }
                    if (index_next_Kontrollpunkt >= 0 && index_next_Kontrollpunkt < points.Count || closed) //weil der erste Kontrollpunkt kein Gegenüber hat, und der letzte  auch net
                    {
                        float abstand = (points[index_von_vorne(index_anker)] - points[index_von_vorne(index_next_Kontrollpunkt)]).magnitude; //returned die Länge des Vektors zwischen den beiden
                        Vector2 richtung = (points[index_von_vorne(index_anker)] - neue_pos).normalized; //normalenvektor und sowas
                        points[index_von_vorne(index_next_Kontrollpunkt)] = points[index_von_vorne(index_anker)] + abstand * richtung; // abstand-mal den Normalenvektor(Länge = 1) 
                    }
                }
            }
        }
    }
    int index_von_vorne(int i) //wenn die Straße geschlossen ist muss der Index vom letzten zum ersten springen können
    {
        return (i + points.Count) % points.Count;
    }
    public bool Closed //closed true/false
    {
        get
        {
            return closed;
        }
        set
        {
            if (closed != value)
            {
                closed = value;
                if (closed)
                {
                    //setzt neue Kontrollpunkte an Start und Ende (selbe Logik wie bei Abschnitt_hinzufügen() )
                    points.Add(points[points.Count - 1] * 2 - points[points.Count - 2]);
                    points.Add(points[0] - (points[1] - points[0]));

                    if (setzen_von_kontrollpunkten)
                    {
                        kontrollpunkte_setzen(0); //ertser anker
                        kontrollpunkte_setzen(points.Count - 3); //letzter (weil geschlossen)
                    }
                }
                else
                {
                    //Reihenfolge ist wichitg
                    points.RemoveAt(points.Count - 2);
                    points.RemoveAt(points.Count - 1);
                    if (setzen_von_kontrollpunkten)
                    {
                        ErstenLetztenKontrollpunkt_setzen(); //weil die hier die einzigen veränderten sind
                    }
                }
            }
        }
    }

    public bool nodesmalen = false;
    public bool Nodesmalen
    {
        get { return nodesmalen;  }
        set
        {
            if( nodesmalen != value)
            {
                nodesmalen = value;
            }
        }
    }

    public void kontrollpunkte_setzen(int index_anker)
    {
        Vector2 anker = points[index_anker];
        Vector2 richtung = Vector2.zero;
        float distanz1 = 0;
        float distanz2 = 0;
        if (index_anker - 3  >= 0 || closed) //vorherigen Anker 
        {
            Vector2 zu_nachbar1 = points[index_von_vorne(index_anker - 3)] - anker; //Vektor von anker1 zu anker2
            distanz1 = zu_nachbar1.magnitude;
            richtung = zu_nachbar1.normalized;
        }
        if (index_anker + 3 >= 0 || closed) //nächster anker
        {
            Vector2 zu_nachbar2 = points[index_von_vorne(index_anker + 3)] - anker; //vektor von anker1 zu anker 3
            distanz2 -= zu_nachbar2.magnitude;
            richtung -= zu_nachbar2.normalized;
        }

        richtung = richtung.normalized;

        for (int i = 0; i < 2; i++) //weil (max)2 Kontrollpukte pro Anker 
        {
            int index_kontrollpunkt = index_anker + i * 2 - 1;
            if ((index_kontrollpunkt >= 0 && index_kontrollpunkt < points.Count || closed) && i == 0)
            {
                points[index_von_vorne(index_kontrollpunkt)] = anker + distanz1 * 0.5f * richtung; //kontrollpunkt1 an die richtige ´Stelle setzen -> siehe Bild "Kontrollpunkte richtig schieben"
            }
            if ((index_kontrollpunkt >= 0 && index_kontrollpunkt < points.Count || closed) && i == 1)
            {
                points[index_von_vorne(index_kontrollpunkt)] = anker + distanz2 * 0.5f * richtung; //Kontrollpunkt2 an die richtige Stelle setzen
            }
        }

    }
    public void ErstenLetztenKontrollpunkt_setzen()
    {
        if (!closed)
        {
            points[1] = (points[0] + points[2]) * 0.5f;
            points[points.Count - 2] = (points[points.Count - 1] + points[points.Count - 3]) * 0.5f;
        }
    }
    public void anker_verschoben(int index_verschoben)
    {
        for (int i = index_verschoben - 3; i <= index_verschoben + 3; i+=3)
        {
            if (i >= 0 && i < points.Count || closed)
            {
                kontrollpunkte_setzen(index_von_vorne(i));
            }
        }
        ErstenLetztenKontrollpunkt_setzen();
    }
    public void alle_kontrollpunkte_setzen()
    {
        ErstenLetztenKontrollpunkt_setzen();
        for (int i = 0; i < points.Count; i+=3)
        {
            kontrollpunkte_setzen(i);
        }
    }
    public bool Setzen_von_kontrollpunkten
    {
        get
        {
            return setzen_von_kontrollpunkten;
        }
        set
        {
            if (setzen_von_kontrollpunkten != value) //neuen Wert bekommen?
            {
                setzen_von_kontrollpunkten = value;
                if (setzen_von_kontrollpunkten) //wenn jetzt true, dann mach
                {
                    alle_kontrollpunkte_setzen();
                }
            }
        }
    }

    public void AbschnittLöschen(int ankerindex) //Punkt/Abschnitt löschen
    {
        if (anzahl_abschnitte > 2 || !closed && anzahl_abschnitte > 1) //nur, wenn noch genug da sind
        {
            if (ankerindex == 0) //Startpunkt
            {
                if (closed)
                {
                    points[points.Count - 1] = points[2]; //wenn zu
                }
                points.RemoveRange(0, 3); //genrell
            }
            else if (ankerindex == points.Count - 1 && !closed) //letzter und zu
            {
                points.RemoveRange(ankerindex - 2, 3);
            }
            else //Standardmäßig immer das
            {
                points.RemoveRange(ankerindex - 1, 3);
            }
        }
    }
    public void punkt_zwischen_schieben(Vector2 neuer_anker, int abschnitt_index)
    {
        points.InsertRange(abschnitt_index * 3 + 2, new Vector2[] { Vector2.zero, neuer_anker, Vector2.zero });
        if (setzen_von_kontrollpunkten)
        {
            anker_verschoben(abschnitt_index * 3 + 3);
        }
        else
        {
            kontrollpunkte_setzen(abschnitt_index * 3 + 3);
        }
    }
    public Vector2[] nodes_berechen(float gewünschter_abstand, float genauigkeit)
    {
        List<Vector2> nodes = new List<Vector2>();
        nodes.Add(points[0]);
        Vector2 letzte_node = points[0];
        float abstand_zum_letzten = 0;


        for (int i = 0; i < anzahl_abschnitte; i++)
        {
            Vector2[] points_abschnitt = get_abschnitt(i);

            float kontrollpunkteabstandzusammen = Vector2.Distance(points_abschnitt[0], points_abschnitt[1]) + Vector2.Distance(points_abschnitt[1], points_abschnitt[2]) + Vector2.Distance(points_abschnitt[2], points_abschnitt[3]);
            float abschnitt_länge = Vector2.Distance(points_abschnitt[0], points_abschnitt[3]) + kontrollpunkteabstandzusammen / 2;
            int Faktor_genauigkeit = (int)(abschnitt_länge * genauigkeit * 15);

            float time = 0;
            while (time <= 1)
            {
                time += 0.1f / Faktor_genauigkeit;
                Vector2 punkt_auf_kurve = BezierKurve.würfelisch(points_abschnitt[0], points_abschnitt[1], points_abschnitt[2], points_abschnitt[3], time);
                abstand_zum_letzten += Vector2.Distance(punkt_auf_kurve, letzte_node);
                while (abstand_zum_letzten >= gewünschter_abstand)
                {
                    float drüber = abstand_zum_letzten - gewünschter_abstand;
                    Vector2 neue_node = punkt_auf_kurve - (letzte_node - punkt_auf_kurve).normalized * drüber;
                    nodes.Add(neue_node);
                    abstand_zum_letzten = drüber;
                    letzte_node = neue_node;
                }
                letzte_node = punkt_auf_kurve;
            }
        }
        return nodes.ToArray();
    }
}
