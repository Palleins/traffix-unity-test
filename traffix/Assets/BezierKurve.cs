﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BezierKurve
{
    public static Vector2 quadratisch(Vector2 a, Vector2 b, Vector2 c, float time)
    {
        Vector2 punkt0 = Vector2.Lerp(a, b, time);
        Vector2 punkt1 = Vector2.Lerp(b, c, time);
        return Vector2.Lerp(punkt0, punkt1, time);
    }
    public static Vector2 würfelisch(Vector2 a, Vector2 b, Vector2 c, Vector2 d, float time)
    {
        Vector2 punkt0 = quadratisch(a, b, c, time);
        Vector2 punkt1 = quadratisch(b, c, d, time);
        return Vector2.Lerp(punkt0, punkt1, time);
    }
}
